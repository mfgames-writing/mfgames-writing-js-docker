FROM ubuntu:19.04
MAINTAINER Dylan R. E. Moonfire <d.moonfire@mfgames.com>

# Ubuntu needs to have the time zone information configured.
ENV TZ=UTC
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Grab the core programs we need to run the rest of this.
RUN apt-get update -y \
    && apt-get install -y \
        awscli \
        build-essential \
        curl \
        epubcheck \
        git \
        libcairo2-dev \
        libffi-dev \
        libgdk-pixbuf2.0-0 \
        libpango-1.0-0 \
        libpangocairo-1.0-0 \
        pandoc \
        pdftk \
        python3-pip \
        s3cmd \
        wget \
    && rm -rf /var/lib/apt/lists/*

# We want to grab Node 12, the latest LTS. We also want the latest node-gyp
# installed because otherwise it takes a longer time to build in CI.
RUN curl -sL https://deb.nodesource.com/setup_12.x \
    | bash - \
    && apt-get install -y \
        nodejs \
    && npm install --global npm@latest \
    && rm -rf /var/lib/apt/lists/*
RUN npm install --global node-gyp

# Download kindlegen and install it to /usr/bin
RUN wget http://kindlegen.s3.amazonaws.com/kindlegen_linux_2.6_i386_v2_9.tar.gz -O /tmp/kindlegen.tar.gz \
    && tar -xzf /tmp/kindlegen.tar.gz -C /tmp \
    && mv /tmp/kindlegen /usr/bin \
    && rm -rf /tmp/*

# Install WeasyPrint
RUN pip3 install WeasyPrint

mfgames-writing-js-docker
=========================

A docker image that contains the required programs for generating files using `mfgames-writing-js` and associated libraries.

This includes the following applications:

* epubcheck
* kindlegen v2.9
* pandoc
* awscli
* WeasyPrint

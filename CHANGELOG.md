## [1.1.1](https://gitlab.com/mfgames-writing/mfgames-writing-js-docker/compare/v1.1.0...v1.1.1) (2018-08-02)


### Bug Fixes

* fixing image versions ([3174e6c](https://gitlab.com/mfgames-writing/mfgames-writing-js-docker/commit/3174e6c))

# [0.2.0](https://gitlab.com/mfgames-writing/mfgames-writing-js-docker/compare/v0.1.0...v0.2.0) (2018-08-02)


### Features

* added Pandoc to the image ([dcf1240](https://gitlab.com/mfgames-writing/mfgames-writing-js-docker/commit/dcf1240))
